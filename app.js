var app = new Vue({
  el: "#app",
  data: {
    kataKunci: "",
    books: ["Kalkulus Dasar", "Fisika Dasar", "Aljabar Linier Elementer", "Aljabar Abstrak", "Algoritma Pemograman"],
  },
  computed: {
    seleksi: function () {
      const res = this.books.filter(book => book.includes(this.kataKunci));
      return res;
    },
  },
});
